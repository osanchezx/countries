import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CountryService } from 'src/app/services/country.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {
  countries: any;
  error: any;
  page: number;
  searchForm!: FormGroup;
  load: boolean = true;
  @ViewChild("modalMensajeErr") modalErr: ElementRef;

  constructor(private countryService: CountryService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getCountries();
    this.initForm();
  }

  /* Metodo encargado de consultar todos los paises */
  getCountries(){
    this.countryService.getCountries().subscribe(
      data => {
        var respStr = JSON.stringify(data);
        const resp = JSON.parse(respStr);
        this.countries = resp;
        this.load = false;
      },
      err => {
        this.error = err.error.message;
        this.modalService.open(this.modalErr);
      }
    );
  }

  /* Metodo encargado de cerrar el modal de error */
  closeModal(){
    this.modalService.dismissAll();
  }

  /* Metodo encargado de inicializar el formulario encargado de realizar la busqueda de un pais */
  private initForm() {
    let name = '';

    this.searchForm = new FormGroup({
      'name': new FormControl(name, [Validators.required, Validators.pattern("[a-zA-Z ]*")]),
    });
  }

  /* Metodo encargado de realizar una busqueda de un pais tomando como referencia su nombre */
  searchByName(){
    this.load = true;
    this.countryService.searchByCountry(this.searchForm.value['name']).subscribe(
      data => {
        var respStr = JSON.stringify(data);
        const resp = JSON.parse(respStr);
        this.countries = resp;
        this.load = false;
      },
      err => {
        this.error = err.error.message;
        this.modalService.open(this.modalErr);
      }
    );
  }

  /* Metodo encargado de berificar si el input de busqueda contiene información */
  inputChange(arg) {
    if (arg == ""){
      this.load = true;
      this.getCountries();
    }
  }

}
