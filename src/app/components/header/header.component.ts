import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  /* Metodo encargado de redirigir a una ruta */
  redirect(){
    this.router.navigate(['/']).then(() => {
    window.location.reload();
  });
  }
}
