import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CountryService } from 'src/app/services/country.service';

@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.css']
})
export class CountryDetailComponent implements OnInit {
  code: string;
  country: any;
  language_keys: any;
  currency_keys: any;
  error: any;
  load: boolean = true;
  @ViewChild("modalMensajeErr") modalErr: ElementRef;

  constructor(private countryService: CountryService, private modalService: NgbModal, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.code = params['code'];
      this.getCountry();
    });
  }

  /* Metodo encargado de obtener la informacion de un pais tomandoc omo referencia su codigo */
  getCountry(){
    this.countryService.searchByCode(this.code).subscribe(
      data => {
        var respStr = JSON.stringify(data);
        const resp = JSON.parse(respStr);
        this.country = resp[0];
        this.language_keys = Object.keys(this.country.languages)
        this.currency_keys = Object.keys(this.country.currencies)
        this.load = false;
      },
      err => {
        this.error = err.error.message;
        this.modalService.open(this.modalErr);
      }
    );
  }

  /* Metodo encargado de cerrar el modal de error */
  closeModal(){
    this.modalService.dismissAll();
  }
}
