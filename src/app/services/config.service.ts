import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'
  })
};


@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  public config: any;

  constructor(private httpClient: HttpClient) { }

  /* Metodo encargado de cargar el json que contiene la informacion de los endpoints de los servicios */
  getConfig(): Promise<any> {
    let location = 'assets/config/settings.json';
    const promise = this.httpClient.get<any>(location, httpOptions)
      .toPromise()
      .then(settings => {
        this.config = settings;
        return this.config;
      });
    return promise;
  }
}
