import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CountryService {
  private server: string;
  private path: string;

  constructor(private http: HttpClient, private configService: ConfigService) { }

  /* Metodo encargado de formar el endpoint para consumir el servicio encargado de obtener todos los paises */
  getCountries(): Observable<any> {
    this.server = this.configService.config.settings.server;
    this.path = this.configService.config.settings.getCountries.get;
    return this.http.get(this.server+this.path, httpOptions);
  }

  /* Metodo encargado de formar el endpoint para consumir el servicio encargado de buscar un pais tomando como referencia su nombre */
  searchByCountry(name:string): Observable<any> {
    this.server = this.configService.config.settings.server;
    this.path = this.configService.config.settings.searchByCountry.get;
    return this.http.get(this.server+this.path+name, httpOptions);
  }

  /* Metodo encargado de formar el endpoint para consumir el servicio encargado de buscar un pais tomando como referencia su codigo*/
  searchByCode(code:string): Observable<any> {
    this.server = this.configService.config.settings.server;
    this.path = this.configService.config.settings.searchByCode.get;
    return this.http.get(this.server+this.path+code, httpOptions);
  }
}
