import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryDetailComponent } from './components/country-detail/country-detail.component';
import { CountryListComponent } from './components/country-list/country-list.component';

const routes: Routes = [
  {
    path: "",
    component: CountryListComponent,
  },
  {
    path: "country-list",
    component: CountryListComponent,
  },
  {
    path: "country-detail/:code",
    component: CountryDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
